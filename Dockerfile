FROM openjdk:8-alpine
ARG MICROSERVICE
ARG PORT
ARG PROFILE
ENV port_var=$PORT
ENV profile_var=$PROFILE
ADD  /target/$MICROSERVICE-0.0.1-SNAPSHOT.jar /usr/share/app.jar
RUN echo $port_var
RUN echo $profile_var
ENTRYPOINT ["/usr/bin/java", "-jar","/usr/share/app.jar","--port=${port_var}","--profile=${profile_var}"]
#EXPOSING PORT
EXPOSE 8080
