package com.punkubiz.guia.service;

import com.punkubiz.guia.dto.request.GuideFilterRequest;
import com.punkubiz.guia.dto.request.GuideMatchRequest;
import com.punkubiz.guia.dto.response.GuideDetailResponse;
import com.punkubiz.guia.dto.response.GuideResponse;
import com.punkubiz.guia.exceptions.custom.CustomNotFoundException;
import com.punkubiz.guia.exceptions.custom.CustomValidationException;
import com.punkubiz.guia.model.*;
import com.punkubiz.guia.repository.*;
import com.punkubiz.guia.util.Util;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GuideService {
    private final GuideRepository guideRepository;
    private final GuideDetailRepository guideDetailRepository;
    private final DocumentRepository documentRepository;
    private final DocumentDetailRepository documentDetailRepository;
    private final MatchingRepository matchingRepository;
    private final MatchingDetailRepository matchingDetailRepository;

    private void validateParameter(GuideFilterRequest parameter) {
        if (parameter.getIndex() == null) parameter.setIndex(0);
        if (parameter.getSize() == null) parameter.setSize(10);
        if (parameter.getFechaDesde() == null) parameter.setFechaDesde(LocalDate.now().minusMonths(6));
        if (parameter.getFechaHasta() == null) parameter.setFechaHasta(LocalDate.now());

        /* Validar rango de fechas permitido. */
        if (parameter.getFechaDesde().isBefore(parameter.getFechaHasta().minusMonths(6)))
            throw new CustomValidationException("El rango de fechas excede lo permitido (6 meses).");
    }

    public Page<GuideResponse> findByFilter(String rol,
                                            String ruc,
                                            GuideFilterRequest parameter) {
        validateParameter(parameter);

        /* Listar las OC por filtro. */
        Page<GuideResponse> result = guideRepository.findByFilter(rol, ruc, parameter);
        if (result == null || result.getContent().isEmpty()) throw new CustomNotFoundException();

        return result;
    }

    public Page<GuideResponse> findWithoutDocument(String rol,
                                                   String ruc,
                                                   GuideFilterRequest parameter) {
        validateParameter(parameter);

        /* Listar las OC por filtro. */
        Page<GuideResponse> result = guideRepository.findWithoutDocument(rol, ruc, parameter);
        if (result == null || result.getContent().isEmpty()) throw new CustomNotFoundException();

        return result;
    }

    public List<GuideDetailResponse> findDetailsById(String id) {
        /* Listar los items de la Guia por su Id. */
        List<GuideDetailResponse> list = guideDetailRepository.findByIdGuia(id);
        if (list == null || list.isEmpty()) throw new CustomNotFoundException();

        return list;
    }

    public String matchDocument(String id,
                                GuideMatchRequest document) {
        /* Verificar si existe la guía. */
        Guide guide = guideRepository.findById(id);
        if (guide == null) throw new CustomNotFoundException("No se encontró la guía seleccionada.");

        if (Boolean.TRUE.equals(document.getChecked())) {
            /* Verificar si el comprobante está asociado a otra guía. */
            Document doc = documentRepository.findById(document.getIdComprobante());
            if (doc.getNroGuiaEntrada() != null && !doc.getNroGuiaEntrada().equals(guide.getNroGuiaEntrada()))
                throw new CustomValidationException("Este comprobante ya está asociado a otra guía.");

            /* Asociar guía al comprobante. */
            documentRepository.associate(document, guide);

            /* Asociar comprobantes a la guía. */
            guideRepository.associate(id, document);

            /* Registrar matching en caso el total del comprobante sea diferente al de la guía. */
            Document data = documentRepository.findById(document.getIdComprobante());
            int val = guide.getValorTotal().compareTo(data.getValorTotal());
            int indMatch = Util.getIndMatch(val);
            if (indMatch > 1) registrarMatching(data, guide, indMatch);
        } else {
            /* Desasociar guía al comprobante. */
            documentRepository.disassociate(document);

            /* Desasociar comprobante a la guía. */
            guideRepository.disassociate(id, document);

            /* Eliminar matching */
            Document data = documentRepository.findById(document.getIdComprobante());
            matchingRepository.delete(data.getId(), guide.getId());
        }

        return "Se procesó correctamente.";
    }

    private void registrarMatching(Document document, Guide guide, int indMatch) {
        Matching matching = new Matching(document, guide);
        matching.setIndMatch(indMatch);
        Matching tmpMatching = matchingRepository.insert(matching);

        List<DocumentDetail> itemsDocument = documentDetailRepository.findByIdComprobante(document.getId());
        List<GuideDetail> itemsGuide = guideDetailRepository.findByGuia(guide.getId());

        List<MatchingDetail> items = new MatchingDetail().getItems(itemsDocument, itemsGuide);
        items.forEach(x -> x.setIdMatching(tmpMatching.getId()));
        matchingDetailRepository.insert(items);
    }
}
