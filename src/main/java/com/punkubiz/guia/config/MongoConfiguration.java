package com.punkubiz.guia.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import java.util.concurrent.TimeUnit;

@Configuration
public class MongoConfiguration {
    private final SSLContext context;
    @Value("${spring.data.mongodb.uri}")
    private String connString;
    @Value("2")
    private int minConnections;
    @Value("10")
    private int maxConnections;

    @Autowired
    public MongoConfiguration(SSLContext context) {
        this.context = context;
    }

    @Bean
    public MongoClientSettings mongoClientSettings() {
        return MongoClientSettings.builder()
                .applyToSslSettings(
                        builder -> builder
                                .enabled(true)
                                .context(context))
                .applyToServerSettings(
                        builder -> builder
                                .heartbeatFrequency(10000, TimeUnit.MILLISECONDS)
                                .minHeartbeatFrequency(500, TimeUnit.MILLISECONDS)
                )
                .applyConnectionString(
                        new ConnectionString(connString)
                )
                .build();
    }
}