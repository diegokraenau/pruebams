package com.punkubiz.guia.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "client")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idEmpresa;
    private String rucEmpresa;
    private String nombreEmpresa;
    private Integer estadoCliente = 0;
    private Integer statusTransferencia = 3;
    private Integer tipoMatch = 0;
    private Integer tiempoRechazo = 120;
    private Boolean permiteCmayor = false;
    private Long fechaCreacion;
    private Long fechaActualizacion;
}
