package com.punkubiz.guia.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.model.herencia.Monto;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@org.springframework.data.mongodb.core.mapping.Document(collection = "document")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Document extends Monto {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    private String tipoDocumento;
    private String numeroDocumento;
    private Long fechaEmision;
    private Long fechaVencimiento;
    private Integer origenDoc;
    private String rucEmisor;
    private String nombreEmisor;
    private String correoEmisor;
    private String paisEmisor;
    private String direccionEmisor;
    private String ciudadEmisor;
    private String ubigeoEmisor;
    private String rucAdquiriente;
    private String nombreAdquiriente;
    private String paisAdq;
    private String direccionAdquiriente;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idOrdenCompra;
    private String nroOrdenCompra;
    private Long fechaOrdenCompra;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idGuiaEntrada;
    private String nroGuiaEntrada;
    private Long fechaGuiaEntrada;
    private Integer indMatch;
    private Integer formaPago;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoIsc;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoIgv;
    private String coDetraccion;
    private Integer status;
    private Long fechaActualiza;
    private Long fechaProceso;
    private Integer estadoCp;
    private Integer motivoRechazo;
    private Integer statusRechazo;
    private Integer facturaLogistica;
    private Integer facturaTransferida;
    private Long fechaTransferida;
    private String urlUbl;
    private String urlPdf;
    private String urlAdjunto;
    private List<Fee> cuotas;

    private Long fechaRegistrada;
    private Integer estadoPago;
    private Integer formaPagoReal;
    private String numCertificado;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoDetraccion;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoRetencion;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoOtraDeduccion;
    private Integer tipoFinanciamiento;
    private Integer tipoConfirming;
    private String rucEntidadFin;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idEmpresa;
    private String ctaCliente;
    private String ctaProveedor;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal gastosFinancieros;
    private Long fechaCobranza;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal tasaInteres;
}
