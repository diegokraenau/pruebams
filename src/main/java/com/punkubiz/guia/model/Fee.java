package com.punkubiz.guia.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class Fee {
    private Integer cuota;
    private Long fechaVcmto;
    private String moneda;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoCuota;
    private Long fechaPago;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal montoPago;
    private String voucherPago;
    private Integer statusPago;
}
