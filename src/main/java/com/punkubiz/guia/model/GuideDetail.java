package com.punkubiz.guia.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;

@Document(collection = "guideItem")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GuideDetail {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idGuia;
    private Integer item;
    private String codigo;
    private String descripcion;
    private String unidadMedida;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal cantidad;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorUnitario;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorTotal;
    private String tipoItem;
    private Long fechaEntregaItem;
}
