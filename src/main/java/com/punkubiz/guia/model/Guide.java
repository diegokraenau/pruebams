package com.punkubiz.guia.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.model.herencia.Monto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Document(collection = "guide")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Guide extends Monto {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    private Long fechaEmisionGuia;
    private Integer tipoGuia;
    private String nroGuiaEntrada;
    private String rucAdquiriente;
    private String nombreAdquiriente;
    private String rucProveedor;
    private String nombreProveedor;
    private String paisProveedor;
    private String direccionProveedor;
    private String ciudadProveedor;
    private String ubigeoProveedor;
    private String correoProveedor;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idOrdenCompra;
    private String nroOrdenCompra;
    private Long fechaEntrega;
    private String condicionEntrega;
    private String lugarEntrega;
    private String ciudadEntrega;
    private String ubigeoEntrega;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal igv;
    private Integer validacion;
    private Integer statusGuia;
    private List<Doc> comprobantes;
    private Long fechaActualiza;
}