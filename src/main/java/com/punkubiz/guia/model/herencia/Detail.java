package com.punkubiz.guia.model.herencia;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;

@Data
public class Detail {
    private Integer item;
    private String codigoProducto;
    private String descripcion;
    private String unidadMedida;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal cantidad;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorUnitario;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorTotal;
    private String tipoItem;
}
