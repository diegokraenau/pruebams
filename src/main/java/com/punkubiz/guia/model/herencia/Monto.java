package com.punkubiz.guia.model.herencia;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;

@Data
public class Monto {
    private String moneda;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorOpGravada;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorOpExonerada;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorOpInafecta;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorOpGratuita;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorTotal;
}
