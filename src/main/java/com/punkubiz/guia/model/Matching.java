package com.punkubiz.guia.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.util.Util;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Document(collection = "matching")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Matching {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    private Integer indMatch;
    private Long fechaCreacion;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idComprobante;
    private String tipoDocumento;
    private String numeroDocumento;
    private Long fechaEmision;
    private Long fechaVencimiento;
    private String rucEmisor;
    private String nombreEmisor;
    private String correoEmisor;
    private String paisEmisor;
    private String rucAdquiriente;
    private String nombreAdquiriente;
    private String paisAdquiriente;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idOrdenCompra;
    private String nroOrdenCompra;
    private Long fechaOrdenCompra;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idGuiaEntrada;
    private String nroGuiaEntrada;
    private Long fechaGuiaEntrada;
    private String moneda;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorTotalCP;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorTotalGuia;
    private Integer status;
    private Integer accion;
    private Integer motivoAccion;
    private Long fechaActualiza;

    public Matching(com.punkubiz.guia.model.Document document, Guide guide) {
        this.fechaCreacion = Util.convertLocalDateTimeToMilliseconds(LocalDateTime.now());
        this.idComprobante = document.getId();
        this.tipoDocumento = document.getTipoDocumento();
        this.numeroDocumento = document.getNumeroDocumento();
        this.fechaEmision = document.getFechaEmision();
        this.fechaVencimiento = document.getFechaVencimiento();
        this.rucEmisor = document.getRucEmisor();
        this.nombreEmisor = document.getNombreEmisor();
        this.correoEmisor = document.getCorreoEmisor();
        this.paisEmisor = document.getPaisEmisor();
        this.rucAdquiriente = document.getRucAdquiriente();
        this.nombreAdquiriente = document.getNombreAdquiriente();
        this.paisAdquiriente = document.getPaisAdq();
        this.idOrdenCompra = document.getIdOrdenCompra();
        this.nroOrdenCompra = document.getNroOrdenCompra();
        this.fechaOrdenCompra = document.getFechaOrdenCompra();
        this.idGuiaEntrada = guide.getId();
        this.nroGuiaEntrada = guide.getNroGuiaEntrada();
        this.fechaGuiaEntrada = guide.getFechaEmisionGuia();
        this.moneda = document.getMoneda();
        this.valorTotalCP = document.getValorTotal();
        this.valorTotalGuia = guide.getValorTotal();
        this.status = document.getStatus();
    }
}
