package com.punkubiz.guia.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.model.herencia.Detail;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Document(collection = "matchingItem")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatchingDetail extends Detail {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idMatching;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal cantidadGuia;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorUnitarioGuia;
    @Field(targetType = FieldType.DECIMAL128)
    private BigDecimal valorTotalGuia;

    public MatchingDetail(DocumentDetail itemDocument, GuideDetail itemGuide) {
        this.setItem(itemDocument.getItem());
        this.setCodigoProducto(itemDocument.getCodigoProducto());
        this.setDescripcion(itemDocument.getDescripcion());
        this.setCantidad(itemDocument.getCantidad());
        this.setUnidadMedida(itemDocument.getUnidadMedida());
        this.setValorUnitario(itemDocument.getValorUnitario());
        this.setValorTotal(itemDocument.getValorTotal());
        this.setTipoItem(itemDocument.getTipoItem());
        this.cantidadGuia = itemGuide == null ? BigDecimal.valueOf(0) : itemGuide.getCantidad();
        this.valorUnitarioGuia = itemGuide == null ? BigDecimal.valueOf(0) : itemGuide.getValorUnitario();
        this.valorTotalGuia = itemGuide == null ? BigDecimal.valueOf(0) : itemGuide.getValorTotal();
    }

    public List<MatchingDetail> getItems(List<DocumentDetail> itemsDocument, List<GuideDetail> itemsGuide) {
        List<MatchingDetail> items = new ArrayList<>();

        for (DocumentDetail itemDocument : itemsDocument) {
            GuideDetail itemGuide = itemsGuide.stream().filter(x -> x.getCodigo().equals(itemDocument.getCodigoProducto())).findFirst().orElse(null);
            items.add(new MatchingDetail(itemDocument, itemGuide));
        }

        return items;
    }
}
