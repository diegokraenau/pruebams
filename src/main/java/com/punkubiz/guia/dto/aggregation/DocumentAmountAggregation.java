package com.punkubiz.guia.dto.aggregation;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DocumentAmountAggregation {
    private BigDecimal valorTotal;
}
