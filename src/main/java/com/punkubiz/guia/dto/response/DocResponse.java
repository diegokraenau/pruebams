package com.punkubiz.guia.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.model.Doc;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocResponse {
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idComprobante;
    private String numeroComprobante;
    private boolean checked = true;

    public List<DocResponse> getComprobantes(List<Doc> docs) {
        List<DocResponse> response = new ArrayList<>();
        if (docs != null) {
            for (Doc doc : docs) {
                response.add(new DocResponse(doc.getIdComprobante(), doc.getNumeroComprobante(), true));
            }
        }

        return response;
    }
}
