package com.punkubiz.guia.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.model.Guide;
import com.punkubiz.guia.util.Util;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class GuideResponse {
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    private String fechaEmisionGuia;
    private int tipoGuia;
    private String nroGuiaEntrada;
    private String rucAdquiriente;
    private String nombreAdquiriente;
    private String rucProveedor;
    private String nombreProveedor;
    private String pais;
    private String correoProveedor;
    private String direccionProveedor;
    private String ciudadProveedor;
    private String ubigeoProveedor;
    private String condicionEntrega;
    private String lugarEntrega;
    private String ciudadEntrega;
    private String ubigeoEntrega;
    private String fechaEntrega;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idOrdenCompra;
    private String nroOrdenCompra;
    private String moneda;
    private String valorOpGravada;
    private String valorOpExonerada;
    private String valorOpInafecta;
    private String valorOpGratuita;
    private String valorVenta;
    private String igv;
    private String valorTotal;
    private List<DocResponse> comprobantes;

    public GuideResponse(Guide data) {
        this.id = data.getId();
        this.fechaEmisionGuia = Util.convertMillisecondsToLocalDateString(data.getFechaEmisionGuia());
        this.tipoGuia = data.getTipoGuia();
        this.nroGuiaEntrada = data.getNroGuiaEntrada();
        this.rucAdquiriente = data.getRucAdquiriente();
        this.nombreAdquiriente = data.getNombreAdquiriente();
        this.rucProveedor = data.getRucProveedor();
        this.nombreProveedor = data.getNombreProveedor();
        this.pais = data.getPaisProveedor();
        this.correoProveedor = data.getCorreoProveedor();
        this.direccionProveedor = data.getDireccionProveedor();
        this.ciudadProveedor = data.getCiudadProveedor();
        this.ubigeoProveedor = data.getUbigeoProveedor();
        this.condicionEntrega = data.getCondicionEntrega();
        this.lugarEntrega = data.getLugarEntrega();
        this.ciudadEntrega = data.getCiudadEntrega();
        this.ubigeoEntrega = data.getUbigeoEntrega();
        this.fechaEntrega = Util.convertMillisecondsToLocalDateString(data.getFechaEntrega());
        this.idOrdenCompra = data.getIdOrdenCompra();
        this.nroOrdenCompra = data.getNroOrdenCompra();
        this.moneda = data.getMoneda();
        this.valorOpGravada = Util.formatCurrency(data.getValorOpGravada());
        this.valorOpExonerada = Util.formatCurrency(data.getValorOpExonerada());
        this.valorOpInafecta = Util.formatCurrency(data.getValorOpInafecta());
        this.valorOpGratuita = Util.formatCurrency(data.getValorOpGratuita());
        this.igv = Util.formatCurrency(data.getIgv());
        this.valorTotal = Util.formatCurrency(data.getValorTotal());
        this.comprobantes = new DocResponse().getComprobantes(data.getComprobantes());

        BigDecimal valIgv = Util.roundDecimals(data.getIgv(), 2);
        BigDecimal valValorTotal = Util.roundDecimals(data.getValorTotal(), 2);
        this.valorVenta = Util.formatCurrency(valValorTotal.subtract(valIgv));
    }
}
