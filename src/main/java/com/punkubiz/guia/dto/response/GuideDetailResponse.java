package com.punkubiz.guia.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.punkubiz.guia.model.GuideDetail;
import com.punkubiz.guia.util.Util;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
public class GuideDetailResponse {
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId idGuia;
    private int item;
    private String codigo;
    private String descripcion;
    private String unidadMedida;
    private String cantidad;
    private String valorUnitario;
    private String valorTotal;

    public GuideDetailResponse(GuideDetail data) {
        this.id = data.getId();
        this.idGuia = data.getIdGuia();
        this.item = data.getItem();
        this.codigo = data.getCodigo();
        this.descripcion = data.getDescripcion();
        this.unidadMedida = data.getUnidadMedida();
        this.cantidad = Util.stringRoundDecimals(data.getCantidad(), 2);
        this.valorUnitario = Util.formatCurrency(data.getValorUnitario());
        this.valorTotal = Util.formatCurrency(data.getValorTotal());
    }
}
