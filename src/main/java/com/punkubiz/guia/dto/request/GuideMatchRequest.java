package com.punkubiz.guia.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class GuideMatchRequest {
    @NotBlank(message = "El campo idComprobante es necesario")
    private String idComprobante;
    @NotBlank(message = "El campo numeroComprobante es necesario")
    private String numeroComprobante;
    @NotNull(message = "El campo checked es necesario")
    private Boolean checked;
}
