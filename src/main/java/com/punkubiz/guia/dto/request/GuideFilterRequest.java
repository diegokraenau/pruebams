package com.punkubiz.guia.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class GuideFilterRequest {
    private Integer index;
    private Integer size;
    private String rucEmpresa;
    private String numeroGuia;
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private LocalDate fechaDesde;
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private LocalDate fechaHasta;
}
