package com.punkubiz.guia.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Util {
    private static final String ZONE = "-05:00";

    private Util() {
        throw new IllegalStateException("Util class");
    }

    public static Long convertLocalDateToMilliseconds(LocalDate date) {
        if (date == null) return null;

        Instant instant = date.atStartOfDay(ZoneId.of(ZoneOffset.of(ZONE).getId())).toInstant();
        return instant.toEpochMilli();
    }

    public static Long convertLocalDateTimeToMilliseconds(LocalDateTime dateTime) {
        if (dateTime == null) return null;

        Instant instant = dateTime.atZone(ZoneId.of(ZoneOffset.of(ZONE).getId())).toInstant();
        return instant.toEpochMilli();
    }

    public static String convertMillisecondsToLocalDateString(Long milliseconds) {
        if (milliseconds == null) return null;

        LocalDate date = Instant.ofEpochMilli(milliseconds).atZone(ZoneId.of(ZoneOffset.of(ZONE).getId())).toLocalDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return date.format(formatter);
    }

    public static BigDecimal roundDecimals(BigDecimal value, int decimals) {
        if (value == null) return null;

        value = value.setScale(decimals, RoundingMode.HALF_EVEN);
        return value;
    }

    public static String stringRoundDecimals(BigDecimal value, int decimals) {
        if (value == null) return null;

        value = value.setScale(decimals, RoundingMode.HALF_EVEN);
        return value.toString();
    }

    public static String formatCurrency(BigDecimal value) {
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        nf.setMinimumFractionDigits(2);
        value = value == null ? BigDecimal.valueOf(0) : value;
        return nf.format(value);
    }

    public static int getIndMatch(int val) {
        if (val == 0) return 1;
        return val > 0 ? 2 : 3;
    }
}
