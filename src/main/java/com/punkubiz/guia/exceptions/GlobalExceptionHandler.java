package com.punkubiz.guia.exceptions;

import com.punkubiz.guia.exceptions.custom.*;
import com.punkubiz.guia.exceptions.model.CustomResult;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        Map<String, Object> body = new HashMap<>();

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        body.put("status", status.value());
        body.put("message", !errors.isEmpty() ? "" : ex.getMessage());
        body.put("errors", errors);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    protected ResponseEntity<Object> handlerMissingRequestHeaderException(MissingRequestHeaderException ex,
                                                                          HttpServletRequest req) {
        String header = Objects.requireNonNull(ex.getMessage()).split("'")[1];
        CustomResult res = createException("Debe ingresar el header: " + header + ".", req, HttpStatus.INTERNAL_SERVER_ERROR);
        return builResponseEntity(res);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handlerConstraintViolationException(ConstraintViolationException ex,
                                                                         HttpServletRequest req) {
        CustomResult res = createException(ex.getMessage(), req, HttpStatus.BAD_REQUEST);
        return builResponseEntity(res);
    }

    @ExceptionHandler(RestClientException.class)
    protected ResponseEntity<Object> handlerInvalidFormatException(RestClientException ex,
                                                                   HttpServletRequest req) {
        CustomResult res = createException(ex.getMessage(), req, HttpStatus.INTERNAL_SERVER_ERROR);
        return builResponseEntity(res);
    }


    @ExceptionHandler(CustomNotFoundException.class)
    protected ResponseEntity<Object> handlerCustomNotFoundException(CustomNotFoundException exception,
                                                                    HttpServletRequest req) {
        CustomResult res = createException(exception.getMessage(), req, HttpStatus.NOT_FOUND);
        return builResponseEntity(res);
    }

    @ExceptionHandler(CustomValidationException.class)
    protected ResponseEntity<Object> handlerCustomValidationException(CustomValidationException exception,
                                                                      HttpServletRequest req) {
        CustomResult res = createException(exception.getMessage(), req, HttpStatus.BAD_REQUEST);
        return builResponseEntity(res);
    }

    @ExceptionHandler(CustomInformationException.class)
    protected ResponseEntity<Object> handlerCustomInformationException(CustomInformationException exception,
                                                                       HttpServletRequest req) {
        CustomResult res = createException(exception.getMessage(), req, HttpStatus.OK);
        return builResponseEntity(res);
    }

    @ExceptionHandler(CustomErrorException.class)
    protected ResponseEntity<Object> handlerCustomErrorException(CustomErrorException exception,
                                                                 HttpServletRequest req) {
        CustomResult res = createException(exception.getMessage(), req, HttpStatus.INTERNAL_SERVER_ERROR);
        return builResponseEntity(res);
    }

    @ExceptionHandler(CustomAuthorizationException.class)
    protected ResponseEntity<Object> handlerCustomAuthorizationException(CustomAuthorizationException exception,
                                                                         HttpServletRequest req) {
        CustomResult res = createException(exception.getMessage(), req, HttpStatus.UNAUTHORIZED);
        return builResponseEntity(res);
    }

    private CustomResult createException(String exception,
                                         HttpServletRequest req,
                                         HttpStatus status) {
        CustomResult res = new CustomResult();
        res.setMessage(exception);
        res.setStatus(status);
        res.setPath(req.getContextPath() + req.getServletPath());

        return res;
    }

    private ResponseEntity<Object> builResponseEntity(CustomResult res) {
        return new ResponseEntity<>(res, res.getStatus());
    }
}
