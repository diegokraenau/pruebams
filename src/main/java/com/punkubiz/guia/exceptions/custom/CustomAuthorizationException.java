package com.punkubiz.guia.exceptions.custom;

public class CustomAuthorizationException extends RuntimeException {
    public CustomAuthorizationException() {
        super("No cuenta con autorización.");
    }

    public CustomAuthorizationException(String message) {
        super(message);
    }
}