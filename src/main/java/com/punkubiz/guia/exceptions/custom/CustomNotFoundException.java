package com.punkubiz.guia.exceptions.custom;

public class CustomNotFoundException extends RuntimeException {
    public CustomNotFoundException() {
        super("No se encontraron datos.");
    }

    public CustomNotFoundException(String message) {
        super(message);
    }
}
