package com.punkubiz.guia.exceptions.custom;

public class CustomInformationException extends RuntimeException {
    public CustomInformationException(String message) {
        super(message);
    }
}
