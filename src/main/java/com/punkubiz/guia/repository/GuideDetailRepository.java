package com.punkubiz.guia.repository;

import com.punkubiz.guia.dto.response.GuideDetailResponse;
import com.punkubiz.guia.model.GuideDetail;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class GuideDetailRepository {
    private final MongoTemplate mongoTemplate;

    public List<GuideDetailResponse> findByIdGuia(String id) {
        Query query = new Query(where("idGuia").is(new ObjectId(id)));
        List<GuideDetail> result = mongoTemplate.find(query, GuideDetail.class, "guideItem");

        List<GuideDetailResponse> list = new ArrayList<>();
        result.forEach(data -> list.add(new GuideDetailResponse(data)));

        return list;
    }

    public List<GuideDetail> findByGuia(ObjectId id) {
        Query query = new Query(where("idGuia").is(id));
        return mongoTemplate.find(query, GuideDetail.class, "guideItem");
    }
}
