package com.punkubiz.guia.repository;

import com.punkubiz.guia.dto.request.GuideMatchRequest;
import com.punkubiz.guia.exceptions.custom.CustomValidationException;
import com.punkubiz.guia.model.Client;
import com.punkubiz.guia.model.Document;
import com.punkubiz.guia.model.Guide;
import com.punkubiz.guia.util.Util;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class DocumentRepository {
    private final MongoTemplate mongoTemplate;

    public Document findById(String id) {
        Query query = new Query(where("id").is(new ObjectId(id)));

        return mongoTemplate.findOne(query, Document.class);
    }

    public void associate(GuideMatchRequest request,
                          Guide guide) {
        Document document = mongoTemplate.findById(new ObjectId(request.getIdComprobante()), Document.class);
        if (document != null) {
            Query query = new Query(where("rucEmpresa").is(document.getRucAdquiriente()));
            Client client = mongoTemplate.findOne(query, Client.class);
            int val = guide.getValorTotal().compareTo(document.getValorTotal());
            int indMatch = Util.getIndMatch(val);

            boolean permite = client != null && !client.getPermiteCmayor();
            if (indMatch == 3 && permite)
                throw new CustomValidationException("El cliente no permite que el total del comprobante sea mayor al de guía.");

            document.setIdGuiaEntrada(guide.getId());
            document.setNroGuiaEntrada(guide.getNroGuiaEntrada());
            document.setFechaGuiaEntrada(guide.getFechaEmisionGuia());
            document.setStatus(4);
            document.setIndMatch(indMatch);
            document.setFechaActualiza(Util.convertLocalDateTimeToMilliseconds(LocalDateTime.now()));

            mongoTemplate.save(document);
        }
    }

    public void disassociate(GuideMatchRequest request) {
        Document document = mongoTemplate.findById(new ObjectId(request.getIdComprobante()), Document.class);
        if (document != null) {
            document.setIdGuiaEntrada(null);
            document.setNroGuiaEntrada(null);
            document.setFechaGuiaEntrada(null);
            document.setStatus(document.getIdOrdenCompra() != null ? 3 : 2);
            document.setIndMatch(0);
            document.setFechaActualiza(Util.convertLocalDateTimeToMilliseconds(LocalDateTime.now()));

            mongoTemplate.save(document);
        }
    }
}
