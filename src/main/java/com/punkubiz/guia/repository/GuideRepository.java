package com.punkubiz.guia.repository;

import com.punkubiz.guia.dto.request.GuideFilterRequest;
import com.punkubiz.guia.dto.request.GuideMatchRequest;
import com.punkubiz.guia.dto.response.GuideResponse;
import com.punkubiz.guia.model.Doc;
import com.punkubiz.guia.model.Guide;
import com.punkubiz.guia.util.Util;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class GuideRepository {
    private static final String FECHA_EMISION_GUIA = "fechaEmisionGuia";
    private static final String RUC_ADQUIRIENTE = "rucAdquiriente";
    private static final String RUC_PROVEEDOR = "rucProveedor";
    private static final String NRO_GUIA_ENTRADA = "nroGuiaEntrada";

    private final MongoTemplate mongoTemplate;

    public Page<GuideResponse> findByFilter(String rol,
                                            String ruc,
                                            GuideFilterRequest parameter) {
        Query query = new Query(where(FECHA_EMISION_GUIA)
                .lte(Util.convertLocalDateToMilliseconds(parameter.getFechaHasta()))
                .gte(Util.convertLocalDateToMilliseconds(parameter.getFechaDesde())));

        return getGuides(query, rol, ruc, parameter);
    }

    public Page<GuideResponse> findWithoutDocument(String rol,
                                                   String ruc,
                                                   GuideFilterRequest parameter) {
        Query query = new Query(where(FECHA_EMISION_GUIA)
                .lte(Util.convertLocalDateToMilliseconds(parameter.getFechaHasta()))
                .gte(Util.convertLocalDateToMilliseconds(parameter.getFechaDesde())));
        query.addCriteria(new Criteria().orOperator(where("comprobantes").is(null),
                where("comprobantes").size(0)));

        return getGuides(query, rol, ruc, parameter);
    }

    public Guide findById(String id) {
        Query query = new Query(where("id").is(new ObjectId(id)));
        return mongoTemplate.findOne(query, Guide.class);
    }

    public void associate(String id,
                          GuideMatchRequest document) {
        Guide guide = mongoTemplate.findById(new ObjectId(id), Guide.class);
        if (guide != null) {
            List<Doc> docs = guide.getComprobantes() == null ? new ArrayList<>() : guide.getComprobantes();
            Doc doc = docs.stream().filter(x -> x.getIdComprobante().equals(new ObjectId(document.getIdComprobante()))).findAny().orElse(null);
            if (doc == null) {
                docs.add(new Doc(new ObjectId(document.getIdComprobante()), document.getNumeroComprobante()));

                guide.setComprobantes(docs);
                guide.setStatusGuia(guide.getComprobantes().isEmpty() ? 0 : 1);
                guide.setFechaActualiza(Util.convertLocalDateTimeToMilliseconds(LocalDateTime.now()));

                mongoTemplate.save(guide);
            }
        }
    }

    public void disassociate(String id,
                             GuideMatchRequest document) {
        Guide guide = mongoTemplate.findById(new ObjectId(id), Guide.class);
        if (guide != null) {
            List<Doc> docs = guide.getComprobantes() == null ? new ArrayList<>() : guide.getComprobantes();
            docs.stream().filter(x -> x.getIdComprobante().equals(new ObjectId(document.getIdComprobante()))).findAny().ifPresent(docs::remove);

            guide.setComprobantes(docs);
            guide.setStatusGuia(guide.getComprobantes().isEmpty() ? 0 : 1);
            guide.setFechaActualiza(Util.convertLocalDateTimeToMilliseconds(LocalDateTime.now()));

            mongoTemplate.save(guide);
        }
    }

    private Page<GuideResponse> getGuides(Query query,
                                          String rol,
                                          String ruc,
                                          GuideFilterRequest parameter) {
        if ("PROVEEDOR".equals(rol)) {
            query.addCriteria(where(RUC_PROVEEDOR).is(ruc));
            if (StringUtils.isNoneBlank(parameter.getRucEmpresa()))
                query.addCriteria(where(RUC_ADQUIRIENTE).is(parameter.getRucEmpresa()));
        }
        if ("CLIENTE".equals(rol)) {
            query.addCriteria(where(RUC_ADQUIRIENTE).is(ruc));
            if (StringUtils.isNoneBlank(parameter.getRucEmpresa()))
                query.addCriteria(where(RUC_PROVEEDOR).is(parameter.getRucEmpresa()));
        }
        if ("ADMIN".equals(rol) && StringUtils.isNoneBlank(parameter.getRucEmpresa())) {
            query.addCriteria(where(RUC_ADQUIRIENTE).is(parameter.getRucEmpresa()));
        }

        if (StringUtils.isNoneBlank(parameter.getNumeroGuia()))
            query.addCriteria(where(NRO_GUIA_ENTRADA).is(parameter.getNumeroGuia()));

        long count = mongoTemplate.count(query, Guide.class);
        Pageable pageable = PageRequest.of(parameter.getIndex(), parameter.getSize());
        query.with(Sort.by(Sort.Order.desc(FECHA_EMISION_GUIA)));
        query.with(Sort.by(Sort.Order.asc(RUC_ADQUIRIENTE)));
        query.with(Sort.by(Sort.Order.asc(RUC_PROVEEDOR)));
        query.with(Sort.by(Sort.Order.desc(NRO_GUIA_ENTRADA)));
        query.with(pageable);
        List<Guide> result = mongoTemplate.find(query, Guide.class, "guide");

        List<GuideResponse> list = new ArrayList<>();
        result.forEach(data -> list.add(new GuideResponse(data)));

        return PageableExecutionUtils.getPage(list, pageable, () -> count);
    }
}
