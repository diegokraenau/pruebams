package com.punkubiz.guia.repository;

import com.punkubiz.guia.model.Matching;
import com.punkubiz.guia.model.MatchingDetail;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class MatchingRepository {
    private final MongoTemplate mongoTemplate;

    public Matching insert(Matching matching) {
        return mongoTemplate.insert(matching, "matching");
    }

    public void delete(ObjectId idComprobante, ObjectId idGuia) {
        Query query = new Query(where("idComprobante").is(idComprobante).
                and("idGuiaEntrada").is(idGuia));
        Matching matching = mongoTemplate.findOne(query, Matching.class);
        if (matching != null) {
            Query queryDetail = new Query(where("idMatching").is(matching.getId()));
            mongoTemplate.remove(queryDetail, MatchingDetail.class);
        }
        mongoTemplate.remove(query, Matching.class);
    }
}
