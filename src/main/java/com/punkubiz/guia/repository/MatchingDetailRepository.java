package com.punkubiz.guia.repository;

import com.punkubiz.guia.model.MatchingDetail;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class MatchingDetailRepository {
    private final MongoTemplate mongoTemplate;

    public void insert(List<MatchingDetail> items) {
        mongoTemplate.insert(items, "matchingItem");
    }
}
