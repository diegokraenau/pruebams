package com.punkubiz.guia.repository;

import com.punkubiz.guia.model.DocumentDetail;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class DocumentDetailRepository {
    private final MongoTemplate mongoTemplate;

    public List<DocumentDetail> findByIdComprobante(ObjectId idComprobante) {
        Query query = new Query(where("idComprobante").is(idComprobante));
        return mongoTemplate.find(query, DocumentDetail.class);
    }
}
