package com.punkubiz.guia.controller;

import com.punkubiz.guia.dto.request.GuideFilterRequest;
import com.punkubiz.guia.dto.request.GuideMatchRequest;
import com.punkubiz.guia.dto.response.GuideDetailResponse;
import com.punkubiz.guia.dto.response.GuideResponse;
import com.punkubiz.guia.exceptions.custom.CustomAuthorizationException;
import com.punkubiz.guia.service.GuideService;
import com.punkubiz.guia.util.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Microservicio de Guia.")
@RestController
@RequiredArgsConstructor
@Validated
public class GuideController {
    private final GuideService guideService;

    @GetMapping(path = "/guides")
    public String list() {
        System.out.println("asfasfsafasfwefwf");
        //rol = rol.toUpperCase();
        //f (rol.equals("CLIENTE-API")) throw new CustomAuthorizationException();

        return "kraenau-diego";
       // return guideService.findByFilter(rol, ruc, parameter);
    }

    @GetMapping(path = "/guides/without-document")
    public Page<GuideResponse> listWithoutDocument(@RequestHeader(name = "rol") String rol,
                                                   @RequestHeader(name = "ruc") String ruc,
                                                   GuideFilterRequest parameter) {
        System.out.println("KRAENAU");
        rol = rol.toUpperCase();
        if (rol.equals("CLIENTE-API")) throw new CustomAuthorizationException();

        return guideService.findWithoutDocument(rol, ruc, parameter);
    }

    @GetMapping(path = "/guide/{id}/items")
    public List<GuideDetailResponse> listItems(@PathVariable("id") String id) {
        System.out.println("KRAENAU");
        return guideService.findDetailsById(id);
    }

    @PutMapping(path = "/guide/{id}/match")
    public ResponseEntity<Result<String>> match(@PathVariable("id") String id,
                                                @Valid @RequestBody GuideMatchRequest document) {
        System.out.println("KRAENAU");
        String mensaje = guideService.matchDocument(id, document);
        return ResponseEntity.status(200).body(new Result<>(mensaje));
    }
}
